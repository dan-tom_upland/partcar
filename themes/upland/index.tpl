{*
	* 2007-2016 PrestaShop
	*
	* NOTICE OF LICENSE
	*
	* This source file is subject to the Academic Free License (AFL 3.0)
	* that is bundled with this package in the file LICENSE.txt.
	* It is also available through the world-wide-web at this URL:
	* http://opensource.org/licenses/afl-3.0.php
	* If you did not receive a copy of the license and are unable to
	* obtain it through the world-wide-web, please send an email
	* to license@prestashop.com so we can send you a copy immediately.
	*
	* DISCLAIMER
	*
	* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
	* versions in the future. If you wish to customize PrestaShop for your
	* needs please refer to http://www.prestashop.com for more information.
	*
	*  @author PrestaShop SA <contact@prestashop.com>
	*  @copyright  2007-2016 PrestaShop SA
	*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
	*  International Registered Trademark & Property of PrestaShop SA
	*}




	
	<div id="slider_row">
		<div class="row">
			<div class="col-sm-4">

			</div>

			<div class="col-sm-8">

				{hook h='displayTopColumn' mod="homeslider"}

			</div>
		</div>
	</div>





	{include file="$tpl_dir./our-atuts.tpl"}
	{include file="$tpl_dir./contact-section.tpl"}


	{if isset($HOOK_HOME_TAB_CONTENT) && $HOOK_HOME_TAB_CONTENT|trim}
	<div class="tab-content">{$HOOK_HOME_TAB_CONTENT}</div>
	{/if}








	{hook h="displayHome"}



	<div class="block cars">

		<h4 class="title_block">Cars</h4>
		<div class="block_content">

			<uL>
				<li><a href="#" title=""><img src="{$img_cat_dir}14_thumb.jpg">Aston Martin</a></li>
				<li><a href="#" title=""><img src="{$img_cat_dir}13_thumb.jpg">Ford</a></li>
				<li><a href="#" title=""><img src="{$img_cat_dir}14_thumb.jpg">Aston Martin</a></li>
				<li><a href="#" title=""><img src="{$img_cat_dir}13_thumb.jpg">Ford</a></li>
				<li><a href="#" title=""><img src="{$img_cat_dir}14_thumb.jpg">Aston Martin</a></li>
				<li><a href="#" title=""><img src="{$img_cat_dir}13_thumb.jpg">Ford</a></li>
				<li><a href="#" title=""><img src="{$img_cat_dir}14_thumb.jpg">Aston Martin</a></li>
				<li><a href="#" title=""><img src="{$img_cat_dir}13_thumb.jpg">Ford</a></li>
				<li><a href="#" title=""><img src="{$img_cat_dir}14_thumb.jpg">Aston Martin</a></li>
				<li><a href="#" title=""><img src="{$img_cat_dir}13_thumb.jpg">Ford</a></li>
				<li><a href="#" title=""><img src="{$img_cat_dir}14_thumb.jpg">Aston Martin</a></li>
				<li><a href="#" title=""><img src="{$img_cat_dir}13_thumb.jpg">Ford</a></li>
			</uL>

		</div>
	</div>

	{include file="$tpl_dir./customer-opinion.tpl"}


	