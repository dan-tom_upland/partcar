<div class="block comments">

	<h4 class="title_block">{l s="Customers opinion"}</h4>
	<div class="block_content">

		<div class="row">
			<div class="col-sm-12">
				<h3 class="title_main">What our client says</h3>
			</div>
		</div>


		<div class="col-sm-4">
			<div class="comment-block">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus augue in lorem ornare rutrum. Phasellus pretium accumsan lacus, a lacinia ligula elementum eu. Duis id dui et tellus congue feugiat vel et lectus. Donec arcu nibh, tincidunt eu tortor non, accumsan volutpat enim.</p>
			</div>
			<span class="name">Paweł Nowak</span>
		</div>
		<div class="col-sm-4">
			<div class="comment-block">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus augue in lorem ornare rutrum. Phasellus pretium accumsan lacus, a lacinia ligula elementum eu. Duis id dui et tellus congue feugiat vel et lectus. Donec arcu nibh, tincidunt eu tortor non, accumsan volutpat enim.</p>
			</div>
			<span class="name">Paweł Nowak</span>
		</div>
		<div class="col-sm-4">
			<div class="comment-block">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus augue in lorem ornare rutrum. Phasellus pretium accumsan lacus, a lacinia ligula elementum eu. Duis id dui et tellus congue feugiat vel et lectus. Donec arcu nibh, tincidunt eu tortor non, accumsan volutpat enim.</p>
			</div>
			<span class="name">Paweł Nowak</span>
		</div>

	</div>
</div>