{*
    * 2007-2016 PrestaShop
    *
    * NOTICE OF LICENSE
    *
    * This source file is subject to the Academic Free License (AFL 3.0)
    * that is bundled with this package in the file LICENSE.txt.
    * It is also available through the world-wide-web at this URL:
    * http://opensource.org/licenses/afl-3.0.php
    * If you did not receive a copy of the license and are unable to
    * obtain it through the world-wide-web, please send an email
    * to license@prestashop.com so we can send you a copy immediately.
    *
    * DISCLAIMER
    *
    * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
    * versions in the future. If you wish to customize PrestaShop for your
    * needs please refer to http://www.prestashop.com for more information.
    *
    *  @author PrestaShop SA <contact@prestashop.com>
    *  @copyright  2007-2016 PrestaShop SA
    *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
    *  International Registered Trademark & Property of PrestaShop SA
    *}
    {include file="$tpl_dir./errors.tpl"}
    {if isset($category)}
    {if $category->id AND $category->active}



    {if isset($subcategories)}
    {if (isset($display_subcategories) && $display_subcategories eq 1) || !isset($display_subcategories) }
    <!-- Subcategories -->
    <div class="block cars">

        <h4 class="title_block">{$category->name|escape:'html':'UTF-8'}</h4>
        <div class="block_content">

            <ul class="clearfix">
                {foreach from=$subcategories item=subcategory}
                <li>


                    <a href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'html':'UTF-8'}" title="{$subcategory.name|escape:'html':'UTF-8'}" class="img">
                     {if $subcategory.id_image}
                     {if $subcategory.id_image != "de-default"}

                     <div class="img-cat">
                         <img class="replace-2x" src="{$link->getCatImageLink($subcategory.link_rewrite, $subcategory.id_image, 'medium_default')|escape:'html':'UTF-8'}" alt="{$subcategory.name|escape:'html':'UTF-8'}" width="{$mediumSize.width}" height="{$mediumSize.height}" />
                     </div>
                     {/if}
                     {/if}

                     <div class="name">{$subcategory.name|truncate:125:'...'|escape:'html':'UTF-8'}
                     </div>
                 </a>



             </li>
             {/foreach}
         </ul>
     </div>
 </div>
 {/if}
 {elseif $products}
 <div class="block nav-cat">
    <h4 class="title_block">{$category->name|escape:'html':'UTF-8'}</h4>
    <div class="block_content">
        <div class="row">
          {hook h="displayLeftColumn" mod="blocklayered"}
          <div class="col-sm-3"><div class="name">{l s="Sort by"}</div><div class="con">{include file="./product-sort.tpl"}</div></div>
          <div class="col-sm-3"><div class="name">{l s="Change category"}</div><div class="con con-center"><a href="#" class="btn btn-default btn-cat"><i class="fa fa-th" aria-hidden="true"></i> {l s="Show all categories"}</a></div></div>
      </div>
  </div>
</div>





{include file="./product-list.tpl" products=$products}
<div class="content_sortPagiBar">
    <div class="bottom-pagination-content clearfix col-sm-12">
     {include file="./pagination.tpl" paginationId='bottom'}
 </div>
</div>
{/if}
{/if}
{/if}

{if !isset($products) and !isset($subcategories)}
<p class="alert alert-warning">{l s='This category is currently unavailable.'}</p>
{/if}


</div>
<div class="advert-subpage">
    {include file="$tpl_dir./our-atuts.tpl"}
    {include file="$tpl_dir./contact-section.tpl"}
    {include file="$tpl_dir./customer-opinion.tpl"}
</div>
