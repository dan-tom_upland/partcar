

{if !$treecategories}
<!-- Block blockpanelselection module -->
<div id="blockpanelselection_block_left" class="block blockpanelselection">
	<h4 class="title_block"><span style="color : #233f92;">FIND</span> CAR PARTS</h4>
	
		<center style="padding:4px 0 0 0;"><span style="color:white;font-weight:bold;margin-top:4px;">ENTER REGISTRATION:</span></center>
		
		<!-- START show panel serch by VRM -->
		{if $dysp_panel_vrm}		
		<div style="background: #ce2127; margin: 2px;">								
			<form method="get" action="{$link->getPageLink('search')|escape:'html'}" id="search_vrm_num" >								
				<p class="block_content" >
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_VRM_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="1" />					
					<input type="submit" id="search_button" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />
				</p>
			</form>									
		</div>
		{/if}
		<!-- END show panel serch by VRM -->
		
		<!-- START show panel serch by DENMARK -->
		{if $dysp_panel_denmark}
		<div style="background: #ce2127; margin: 2px;">								
			<form method="get" action="{$link->getPageLink('search')|escape:'html'}" id="search_denmark_num">
				<p class="block_content">
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_Denmark_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="2" />					
					<input type="submit" id="search_button" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />
				</p>
			</form>
		</div>
		{/if}
		<!-- END show panel serch by DENMARK -->
		
		<!-- START show panel serch by FINLAND -->
		{if $dysp_panel_finland}
		<div style="background: #ce2127; margin: 2px;">								
			<form method="get" action="{$link->getPageLink('search')|escape:'html'}" id="search_finland_num">
				<p class="block_content">
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_Finland_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="3" />					
					<input type="submit" id="search_button" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />
				</p>
			</form>
		</div>
		{/if}
		<!-- END show panel serch by FINLAND -->
		
		<!-- START show panel serch by FRANCE -->
		{if $dysp_panel_france}
		<div style="background: #ce2127; margin: 2px;">								
			<form method="get" action="{$link->getPageLink('search')|escape:'html'}" id="search_france_num">
				<p class="block_content">
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_France_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="4" />					
					<input type="submit" id="search_button" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />
				</p>
			</form>
		</div>
		{/if}
		<!-- END show panel serch by FRANCE -->
		
		<!-- START show panel serch by NETHERLANDS -->
		{if $dysp_panel_netherlands}
		<div style="background: #ce2127; margin: 2px;">								
			<form method="get" action="{$link->getPageLink('search')|escape:'html'}" id="search_netherlands_num">
				<p class="block_content">
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_Netherlands_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="5" />					
					<input type="submit" id="search_button" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />													
				</p>
			</form>
		</div>
		{/if}
		<!-- END show panel serch by NETHERLANDS -->
		
		<!-- START show panel serch by NORWAY -->
		{if $dysp_panel_norway}
		<div style="background: #ce2127; margin: 2px;">								
			<form method="get" action="{$link->getPageLink('search')|escape:'html'}" id="search_norway_num">
				<p class="block_content">
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_Norway_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="6" />					
					<input type="submit" id="search_button" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />													
				</p>
			</form>
		</div>
		{/if}
		<!-- END show panel serch by NORWAY -->
		
		<!-- START show panel serch by SWEDEN -->
		{if $dysp_panel_sweden}
		<div style="background: #ce2127; margin: 2px;">								
			<form method="get" action="{$link->getPageLink('search')|escape:'html'}" id="search_sweden_num">
				<p class="block_content">
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_Sweden_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="7" />					
					<input type="submit" id="search_button" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />													
				</p>
			</form>
		</div>
		{/if}
		<!-- END show panel serch by SWEDEN -->
		
		<!-- START show panel serch by KBA -->
		{if $dysp_panel_kba}
		<div style="background: #ce2127; margin: 2px;">								
			<form method="get" action="{$link->getPageLink('search')|escape:'html'}" id="search_kba_num">
				<p class="block_content">
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_KBA_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="8" />					
					<input type="submit" id="search_button" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />													
				</p>
			</form>
		</div>
		{/if}
		<!-- END show panel serch by KBA -->
		
		<!-- START show panel serch by VIN -->
		{if $dysp_panel_vin}
		<div style="background: #ce2127; margin: 2px;">								
			<form method="get" action="{$link->getPageLink('search')|escape:'html'}" id="search_vin_num">
				<p class="block_content">
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_VIN_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="9" />					
					<input type="submit" id="search_button" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />													
				</p>
			</form>
		</div>
		{/if}
		<!-- END show panel serch by VIN -->
		
		<!-- START show panel serch by ITALIAN -->
		{if $dysp_panel_italian}
		<div style="margin-top: 2px;">								
			<form method="get" action="{$link->getPageLink('search')|escape:'html'}" id="search_italian_num">
				<p class="block_content">
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_Italian_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="10" />					
					<input type="submit" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />													
				</p>
			</form>
		</div>
		{/if}
		<!-- END show panel serch by ITALIAN -->
		
		<!-- START show panel serch by ITALIAN -->
		{if $dysp_panel_portugal}
		<div style="background: #ce2127; margin: 2px;">								
			<form method="get" action="{$link->getPageLink('search')|escape:'html'}" id="search_potrugal_num">
				<p class="block_content">
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_Portugal_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="11" />					
					<input type="submit" id="search_button" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />													
				</p>
			</form>
		</div>
		{/if}
		<!-- END show panel serch by ITALIAN -->
		
		<center style="padding:0 0 0 0;margin-top:-5px;"><span style="color:white;font-weight:bold;">----- OR -----</span></center>
		<center style="padding:0 0 0 0;margin-top:-8px;;margin-bottom:4px;"><span style="color:white;font-weight:bold;">SELECT YOUR VEHICLE:</span></center>			
	
	
	<div class="block_content">
		
		<!-- START show panel selection cars -->
		{if $dysp_panelcars}	
			<!-- *********************** -->
					<select id="years_list" onchange="
																document.cookie = 'select_year_car=' + document.getElementById('years_list').value; 
																document.cookie = 'select_id_brand_car=0'; 
																document.cookie = 'select_id_model_car=0'; 
																document.cookie = 'select_id_type_car=0'; 
																getBrands(document.getElementById('years_list').value, {$id_lang}, {$id_shop});
																 ">	
										<option value="0">Select year</option>
									{if $list_years}
									{for $i=0 to count($list_years)-1}
										<option value="{$list_years[$i]|escape:'html'}" {if $list_years[$i] == $select_year_car} selected="selected"{/if}>{$list_years[$i]|escape:'html':'UTF-8'}</option>
									{/for}
									{/if}
							 </select>
			<!-- *********************** -->
					<select id="brand_list" onchange="
																document.cookie = 'select_id_brand_car=' + document.getElementById('brand_list').value; 
																document.cookie = 'select_id_model_car=0'; 
																document.cookie = 'select_id_type_car=0'; 
																getModels(document.getElementById('brand_list').value, document.getElementById('years_list').value, {$id_lang}, {$id_shop});
																 ">
										<option value="0">Select make</option>
									{if $list_brands}
									{foreach from=$list_brands item=list_brand}
										<option value="{$list_brand.id_bra|escape:'html'}" {if $list_brand.id_bra == $select_id_brand_car} selected="selected"{/if}>{$list_brand.nameBrand|escape:'html':'UTF-8'}</option>
									{/foreach}
									{/if}
							 </select>

			<!-- *********************** -->
					<select id="model_list" onchange="
																document.cookie = 'select_id_model_car=' + document.getElementById('model_list').value; 
																document.cookie = 'select_id_type_car=0'; 
																getTypes(document.getElementById('brand_list').value, document.getElementById('model_list').value, document.getElementById('years_list').value, {$id_lang}, {$id_shop});
																 ">
										<option value="0">Select model</option>
									{if $list_models}
									{foreach from=$list_models item=list_model}
										<option value="{$list_model.id_model|escape:'html'}" {if $list_model.id_model == $select_id_model_car} selected="selected"{/if}>{$list_model.nameModel|escape:'html':'UTF-8'}</option>
									{/foreach}
									{/if}
							 </select>

			<!-- *********************** -->
					<select style="width:70%;margin-bottom:10px;" id="type_list" onchange="
																document.cookie = 'select_id_type_car=' + document.getElementById('type_list').value; 
																getUrlcat(document.getElementById('type_list').value);
																">	
										<option value="0">Select type</option>
									{if $list_types}
									{foreach from=$list_types item=list_type}
										<option value="{$list_type.id_type|escape:'html'}" {if $list_type.id_type == $select_id_type_car} selected="selected"{/if}>{$list_type.nameType|escape:'html':'UTF-8'}</option>
									{/foreach}
									{/if}
							 </select>

			<!-- *********************** -->				
								
								<!-- <button id="CleanDataCar" type="button" onclick="
																			document.cookie = 'select_year_car=0'; 
																			document.cookie = 'select_id_brand_car=0'; 
																			document.cookie = 'select_id_model_car=0'; 
																			document.cookie = 'select_id_type_car=0'; 
																			location.reload();
																			" name="" value="">
									Clean >
								</button> -->
								<!-- ------------------ -->
								<button id="OpenCar" type="button" onclick="
																	location.reload();
																	window.location='{if $select_id_type_car > 0}{$link->getCategoryLink((int)$select_id_type_car)}{/if}'" 
																	name="" value=""> 
									Go >
								</button>
			<!-- *********************** -->

		{/if} 
		<!-- END show panel selection cars -->

	</div>
</div>
<!-- /Block blockpanelselection module -->
{/if}


{if $treecategories && $treecategories != ''}
	<!-- BEGIN TREE CATEGORIES -->
	<div id="blockpanelselection_block_left" class="block blockpanelselection" >  
		<h4 style="font-size:20px;" class="title_block">SELECTED VEHICLE</h4>
		<div class="block_content">
			<!-- <p style="padding:0;margin:0;margin-left:3px;"><strong style="color:red;"><u>SELECTED VEHICLE</u> :</strong></p> -->
			<p style="padding:0;margin:0;margin-left:3px;"><strong>Make:</strong> {$nnamebrand}</p>
			<p style="padding:0;margin:0;margin-left:3px;"><strong>Model:</strong> {$nnamemodel}</p>
			<p style="padding:0;margin:0;margin-left:3px;"><strong>Type:</strong> {$nnametype}</p>				
			<!-- --------------------- -->
			<p style="margin-top:3px;">										
				<button id="Deactivecar" type="button" class="button_mini" style="align : center; text-align: center; padding:0; margin-left:3px; width:50%; color: white; background: red;" 
						onclick="document.cookie = 'select_id_type_car=0'; 
								document.cookie = 'select_year_car=0'; 
								document.cookie = 'select_id_brand_car=0'; 
								document.cookie = 'select_id_model_car=0'; 								
								window.location = '{$link->getBaseLink()}'" name="" value="">Change Vehicle</button>
			</p>	
		</div>
	</div>
	<div id="blockpanelselection_block_left" class="block blockpanelselection" >  
		<h4 style="font-size:20px;" class="title_block">   BROWSE CATEGORIES  </h4>
		<div class="block_content">
			<!-- --------------------- -->	
			<form method="get" action="
								javascript: d.openToShowAllNode();
								javascript: d.openToHideNodeExcept(document.getElementById('searchtree').value, true);" >			
				<p class="block_content">
						<input type="hidden" name="" value="search" />					
						<input type="text" id="searchtree" class="textinput_tree" name="tree" style="width:60%" value placeholder="Enter name part" >										
						<input type="submit" id="search_button" class="button_mini" style="width:70px; top: -2px; color: white; background: red;" value="Search >" />
				</p>
			</form>
			<!-- --------------------- -->	
				<p><a href="javascript: d.openAll();">Expand all</a> | <a href="javascript: d.closeAll();">Collapse all</a> | <strong><a href="javascript: d.openToShowAllNode();">Show all parts</a></strong></p>	
					<div class="block_content" id="dtree" style="height:750px;">											
						<script type="text/javascript">									
						{$treecategories}
						</script>
					</div>

			<!-- --------------------- -->
		</div>
	</div>
	<!-- END TREE CATEGORIES -->
{/if}

