# Panel selection block

## About

Displays a block selection. Sample http://demoprestashop.ttc.bovsoft.com/3-catalog-cars

## Contributing

PrestaShop modules are open-source extensions to the PrestaShop e-commerce solution. Everyone is welcome and even encouraged to contribute with their own improvements.

### Requirements

1. block for select cars will work only on based store which will created with help program "CREATOR DATA FOR PRESTASHOP"
2. blocks for search cars according set plate number car for different countries will work only on based store which will created with help program "CREATOR DATA FOR PRESTASHOP"
--------------------
program "CREATOR DATA FOR PRESTASHOP"
- about http://ttc.bovsoft.com/index.html#creatorforprestashop
- download http://ttc.bovsoft.com/download/CreatorDataForPrestashop.exe
- setting http://ttc.bovsoft.com/prestashop.html
- buy http://ttc.bovsoft.com/buy.html#buycreatorforprestashop
--------------------
Service "Determining K-type (typ_id) vehicle according for vehicle registration plates for countries : Denmark, Finland, France, Netherlands, Norway, Sweden, Great Britain, Ireland, and also according for KBA-number (for German) and VIN code vehicle"
- panel for test, from http://ttc.bovsoft.com/index.html#getvrmtoktype
- description http://ttc.bovsoft.com/service/getktype/readme.txt
--------------------

### Process in details

1. Unzip arhive. Copy folder "blockpanelselection" with files in folder "/yourstore/modules/" your store
2. Install "MODULES -> FRONT OFFICE FEATURES -> Panel selection block" (authors "softBOVformat")
3. Configure "Panel selection block" module :
	- enable necessary panels
	- if use service determing K-type according for vehicle registration plates need enter EMAIL and SECURITY CODE fro necessary services (which you can get after paying the required service)
4. for possible work with "service determing K-type according for vehicle registration plates" you must pay to use the service and do changes for some php-pages

	============================================================================
	/yourstore.com/classes/Search.php
	... need add new 2-functions
	
			public static function GetDataSearchForPlateNumber($bovsoftservice, $plnum)
			{
				$emailclient = Configuration::get('BOVSOFTPANEL_DISPLAY_EMAIL');		
				
				switch ((int)$bovsoftservice) {
								case 1:
									$secur_code = Configuration::get('BOVSOFTPANEL_DISPLAY_PN_GBRITAINANDIRELAND_CODE');
									$path_script = 'getktypeforvrm';
									break;
								case 2:
									$secur_code = Configuration::get('BOVSOFTPANEL_DISPLAY_PN_DENMARK_CODE');
									$path_script = 'getktypefornumplatedenmark';
									break;
								case 3:
									$secur_code = Configuration::get('BOVSOFTPANEL_DISPLAY_PN_FINLAND_CODE');
									$path_script = 'getktypefornumplatefinland';
									break;
								case 4:
									$secur_code = Configuration::get('BOVSOFTPANEL_DISPLAY_PN_FRANCE_CODE');
									$path_script = 'getktypefornumplatefrance';
									break;
								case 5:
									$secur_code = Configuration::get('BOVSOFTPANEL_DISPLAY_PN_NETHERLANDS_CODE');
									$path_script = 'getktypefornumplatenetherlands';
									break;
								case 6:
									$secur_code = Configuration::get('BOVSOFTPANEL_DISPLAY_PN_NORWAY_CODE');
									$path_script = 'getktypefornumplatenorway';
									break;
								case 7:
									$secur_code = Configuration::get('BOVSOFTPANEL_DISPLAY_PN_SWEDEN_CODE');
									$path_script = 'getktypefornumplatesweden';
									break;
								case 8:
									$secur_code = Configuration::get('BOVSOFTPANEL_DISPLAY_KBA_CODE');
									$path_script = 'getktypeforkbanum';
									break;
								case 9:
									$secur_code = Configuration::get('BOVSOFTPANEL_DISPLAY_VIN_CODE');
									$path_script = 'getktypeforvincode';
									break;
								case 10:
									$secur_code = Configuration::get('BOVSOFTPANEL_DISPLAY_ITALIAN');
									$path_script = 'getktypefornumplateitalian';
									break;
								case 11:
									$secur_code = Configuration::get('BOVSOFTPANEL_DISPLAY_PORTUGAL');
									$path_script = 'getktypefornumplateportugal';
									break;
							}
				
				$listcars = array();				
				$listcars_id = array();				
				$listcars_shortdatacar = array();	
				if 	((int)$bovsoftservice == 9)
					{$xml = simplexml_load_file('http://141.170.228.129:60000/<securecode>'.$secur_code.'</securecode><vrmnum>'.$plnum.'</vrmnum><emailclient>'.$emailclient.'</emailclient><nameservice>'.$path_script.'</nameservice>');}
					else {$xml = simplexml_load_file('http://141.170.228.129:59050/<securecode>'.$secur_code.'</securecode><vrmnum>'.$plnum.'</vrmnum><emailclient>'.$emailclient.'</emailclient><nameservice>'.$path_script.'</nameservice>');}				
				// $xml = simplexml_load_file('http://141.170.228.129:59050/<securecode>'.$secur_code.'</securecode><vrmnum>'.$plnum.'</vrmnum><emailclient>'.$emailclient.'</emailclient><nameservice>'.$path_script.'</nameservice>');						
				if ($xml && $xml->status == 200 && $xml->data->empty == 'false')
					{				
					foreach ($xml->data->datacar as $datacar) 
							{					
							$listcars_id[] = Search::GetIDCatFromKtype($datacar->ktype);
							$listcars_shortdatacar[] = $datacar->shortdatacar;					
							}
					}					
							
				$listcars=array('id'=>$listcars_id, 'shortdatacar'=>$listcars_shortdatacar);
				return $listcars;
												
			}
			
			public static function GetIDCatFromKtype($ktype)
			{
				if ($ktype)
					{					
					$id_cat = (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue(
							'SELECT id_category
							FROM `'._DB_PREFIX_.'LinkTypPrestaWithTecDoc` 
							WHERE typ_id = '.(int)$ktype.'
							');						
					return $id_cat;
					} else {return 0;};
			}
	
	============================================================================
	/yourstore.com/controllers/front/SearchController.php
	... change procedure initContent(), add new IF-block "elseif (($bovsoftservice = Tools::getValue('bovsoftservice')) && !is_array($bovsoftservice))"
	
			public function initContent()
			{
				...

				if ($this->instant_search && !is_array($query))
				{
					...
				}
				// THERE ADD NEW BLOCK "ELSEIF"
				// *********** START CHANGES *****************
				elseif (($bovsoftservice = Tools::getValue('bovsoftservice')) && !is_array($bovsoftservice))
					{
						$plnum = Tools::getValue('plnum');
						$plnum=trim(str_replace(array('_', '-', '*', '.', ',', ' ', '+', '/', '|'), '', $plnum));
						
						$ListCats = Search::GetDataSearchForPlateNumber($bovsoftservice, $plnum); 				
									
						$this->context->smarty->assign(array(
							'search_plnum' => $plnum,
							'nbProducts' => count($ListCats['id']),
							'listcars_id' => $ListCats['id'],
							'listcars_shortdatacar' => $ListCats['shortdatacar']));					
					}
				// ********** END CHANGES ********************
				elseif (($query = Tools::getValue('search_query', Tools::getValue('ref'))) && !is_array($query))
				{
				...
				}
				...
			}
	
	
	============================================================================	
	/yourstore.com/themes/yourtheme/search.tpl
	... need change the template output for "Search.tpl", for example here will show changes to the default theme "default-bootstrap", where changes show in blocks "<!-- START CHANGES -->" => "<!-- END CHANGES -->"	
	
			...
			{capture name=path}{l s='Search'}{/capture}

			<h1 
			{if isset($instant_search) && $instant_search}id="instant_search_results"{/if} 
			class="page-heading {if !isset($instant_search) || (isset($instant_search) && !$instant_search)} product-listing{/if}">
				{l s='Search'}&nbsp;
				{if $nbProducts > 0}
					<span class="lighter">
						<!-- START CHANGES -->
						{if $search_plnum}
							"{$search_plnum|escape:'html':'UTF-8'}"
						{else}
							"{if isset($search_query) && $search_query}{$search_query|escape:'html':'UTF-8'}{elseif $search_tag}{$search_tag|escape:'html':'UTF-8'}{elseif $ref}{$ref|escape:'html':'UTF-8'}{/if}"
						{/if}
						<!-- END CHANGES -->
					</span>
				{/if}
				...
			</h1>

			{include file="$tpl_dir./errors.tpl"}
			{if !$nbProducts}
				<p class="alert alert-warning">
					<!-- START CHANGES -->
					{if isset($search_query) && $search_query}
						{l s='No results were found for your search'}&nbsp;"{if isset($search_query)}{$search_query|escape:'html':'UTF-8'}{/if}"
					{elseif isset($search_tag) && $search_tag}
						{l s='No results were found for your search'}&nbsp;"{$search_tag|escape:'html':'UTF-8'}"
					{else}
						{if isset($search_plnum) && $search_plnum}
							In our catalog not found data on vehicles
						{else}
							{l s='Please enter a search keyword'}
						{/if}
					{/if}
					<!-- END CHANGES -->
				</p>	
			{else}								
				<!-- START CHANGES -->
				{if isset($listcars_id) && $listcars_id && isset($listcars_shortdatacar) && $listcars_shortdatacar}
					<ul class="clearfix">		
						{for $i=0 to count($listcars_id)-1}
								<li>					
									<a 
										{if $listcars_id[$i] >= 0}
											href="{$link->getCategoryLink($listcars_id[$i])|escape:'html':'UTF-8'}"
										{/if} 
										title="{if $listcars_id[$i] == 0}Our catalog does not contain data on car{else}{$listcars_shortdatacar[$i]|escape:'html':'UTF-8'}{/if}">
										{$listcars_shortdatacar[$i]|escape:'html':'UTF-8'}
									</a> 				
								</li>								
						{/for}
					</ul>
				<!-- END CHANGES -->
				{elseif $braid == ''}
					{$listbrands}
				{else}	 
						{if isset($instant_search) && $instant_search}
							<p class="alert alert-info">
								{if $nbProducts == 1}{l s='%d result has been found.' sprintf=$nbProducts|intval}{else}{l s='%d results have been found.' sprintf=$nbProducts|intval}{/if}
							</p>
						{/if}
						
						<div class="content_sortPagiBar">
							<div class="sortPagiBar clearfix {if isset($instant_search) && $instant_search} instant_search{/if}">
								{include file="$tpl_dir./product-sort.tpl"}
								{if !isset($instant_search) || (isset($instant_search) && !$instant_search)}
									{include file="./nbr-product-page.tpl"}
								{/if}
							</div>
							<div class="top-pagination-content clearfix">
								{include file="./product-compare.tpl"}
								{if !isset($instant_search) || (isset($instant_search) && !$instant_search)}
									{include file="$tpl_dir./pagination.tpl"}
								{/if}
							</div>
						</div>
						{include file="$tpl_dir./product-list.tpl" products=$search_products}
						<div class="content_sortPagiBar">
							<div class="bottom-pagination-content clearfix">
								{include file="./product-compare.tpl"}
								{if !isset($instant_search) || (isset($instant_search) && !$instant_search)}
									{include file="$tpl_dir./pagination.tpl" paginationId='bottom'}
								{/if}
							</div>
						</div>
				{/if}
			{/if}
				
	============================================================================
	/yourstore.com/classes/Category.php
	... need add new function
	
	public static function GetNameActivCategory($id_category = 0)
			{
				$id_lang = (int)Context::getContext()->language->id;
				$id_shop = (int)Context::getContext()->shop->id;
				
				return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
					SELECT cl.name
					FROM `'._DB_PREFIX_.'category_lang` cl
					WHERE cl.`id_category` = '.(int)$id_category.'
						AND cl.`id_shop` = '.(int)$id_shop.' AND cl.`id_lang` = '.(int)$id_lang);
			}
	
	============================================================================
	
	
... If you have a problem with the proper setting or changing the pages I can help you in this, let me know at the email "admin@ttc.bovsoft.com" or skype (liderservis)
