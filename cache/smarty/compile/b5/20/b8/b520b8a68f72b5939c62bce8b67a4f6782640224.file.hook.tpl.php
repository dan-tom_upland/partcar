<?php /* Smarty version Smarty-3.1.19, created on 2017-06-07 16:01:11
         compiled from "/home/cloudpanel/htdocs/partcar.dev/themes/upland/modules/themeszonemanslider/hook.tpl" */ ?>
<?php /*%%SmartyHeaderCode:189360137059380727d73265-83352427%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b520b8a68f72b5939c62bce8b67a4f6782640224' => 
    array (
      0 => '/home/cloudpanel/htdocs/partcar.dev/themes/upland/modules/themeszonemanslider/hook.tpl',
      1 => 1496657079,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '189360137059380727d73265-83352427',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'manufacturers' => 0,
    'manufacturer' => 0,
    'link' => 0,
    'img_manu_dir' => 0,
    'manSize' => 0,
    'items_wide' => 0,
    'items_desktop' => 0,
    'items_desktop_small' => 0,
    'items_tablet' => 0,
    'items_mobile' => 0,
    'tzc_autoplay' => 0,
    'tzc_nav' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59380727d9e5f2_27340723',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59380727d9e5f2_27340723')) {function content_59380727d9e5f2_27340723($_smarty_tpl) {?><?php if (isset($_smarty_tpl->tpl_vars['manufacturers']->value)&&$_smarty_tpl->tpl_vars['manufacturers']->value) {?>
<div class="tz-man-slider">

    
    <h4 class="title_block"><?php echo smartyTranslate(array('s'=>"Manufactures",'mod'=>"themeszonemanslider"),$_smarty_tpl);?>
</h4>
    <!-- Manufacturers list -->
    <div id="owl-man-slider" class="owl-carousel">
        <?php  $_smarty_tpl->tpl_vars['manufacturer'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['manufacturer']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['manufacturers']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['manufacturer']->key => $_smarty_tpl->tpl_vars['manufacturer']->value) {
$_smarty_tpl->tpl_vars['manufacturer']->_loop = true;
?>

        <a href="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getmanufacturerLink($_smarty_tpl->tpl_vars['manufacturer']->value['id_manufacturer'],$_smarty_tpl->tpl_vars['manufacturer']->value['link_rewrite']), ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" title="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['manufacturer']->value['name'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
">
            <img src="<?php echo $_smarty_tpl->tpl_vars['img_manu_dir']->value;?>
<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['manufacturer']->value['id_manufacturer'], ENT_QUOTES, 'UTF-8', true);?>
-medium_default.jpg" alt="" width="<?php echo $_smarty_tpl->tpl_vars['manSize']->value['width'];?>
" height="<?php echo $_smarty_tpl->tpl_vars['manSize']->value['height'];?>
" />
        </a>
        <?php } ?>
    </div>


    <script>
        $(document).ready(function() {
            var m_owl = $("#owl-man-slider");
            m_owl.owlCarousel({
                items : <?php echo $_smarty_tpl->tpl_vars['items_wide']->value;?>
, //10 items above 1000px browser width
                itemsDesktop : [1000,<?php echo $_smarty_tpl->tpl_vars['items_desktop']->value;?>
], //5 items between 1000px and 901px
                itemsDesktopSmall : [900,<?php echo $_smarty_tpl->tpl_vars['items_desktop_small']->value;?>
], // 3 items betweem 900px and 601px
                itemsTablet: [600,<?php echo $_smarty_tpl->tpl_vars['items_tablet']->value;?>
], //2 items between 600 and 0;
                itemsMobile : <?php echo $_smarty_tpl->tpl_vars['items_mobile']->value;?>
, // itemsMobile disabled - inherit from itemsTablet option
                autoPlay: <?php echo $_smarty_tpl->tpl_vars['tzc_autoplay']->value;?>
,
                navigation: <?php echo $_smarty_tpl->tpl_vars['tzc_nav']->value;?>
,
                navigationText: false,
                pagination: false
            });
        });
    </script>
</div>
<?php }?><?php }} ?>
