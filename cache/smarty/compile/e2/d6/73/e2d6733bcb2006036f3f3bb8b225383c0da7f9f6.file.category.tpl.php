<?php /* Smarty version Smarty-3.1.19, created on 2017-06-12 12:34:00
         compiled from "/home/cloudpanel/htdocs/partcar.dev/themes/upland/category.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8277923759380eb0a67ca7-94926084%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e2d6733bcb2006036f3f3bb8b225383c0da7f9f6' => 
    array (
      0 => '/home/cloudpanel/htdocs/partcar.dev/themes/upland/category.tpl',
      1 => 1497263637,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8277923759380eb0a67ca7-94926084',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59380eb0aedf50_61608700',
  'variables' => 
  array (
    'category' => 0,
    'subcategories' => 0,
    'display_subcategories' => 0,
    'subcategory' => 0,
    'link' => 0,
    'mediumSize' => 0,
    'products' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59380eb0aedf50_61608700')) {function content_59380eb0aedf50_61608700($_smarty_tpl) {?>
    <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php if (isset($_smarty_tpl->tpl_vars['category']->value)) {?>
    <?php if ($_smarty_tpl->tpl_vars['category']->value->id&&$_smarty_tpl->tpl_vars['category']->value->active) {?>



    <?php if (isset($_smarty_tpl->tpl_vars['subcategories']->value)) {?>
    <?php if ((isset($_smarty_tpl->tpl_vars['display_subcategories']->value)&&$_smarty_tpl->tpl_vars['display_subcategories']->value==1)||!isset($_smarty_tpl->tpl_vars['display_subcategories']->value)) {?>
    <!-- Subcategories -->
    <div class="block cars">

        <h4 class="title_block"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</h4>
        <div class="block_content">

            <ul class="clearfix">
                <?php  $_smarty_tpl->tpl_vars['subcategory'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['subcategory']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['subcategories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['subcategory']->key => $_smarty_tpl->tpl_vars['subcategory']->value) {
$_smarty_tpl->tpl_vars['subcategory']->_loop = true;
?>
                <li>


                    <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCategoryLink($_smarty_tpl->tpl_vars['subcategory']->value['id_category'],$_smarty_tpl->tpl_vars['subcategory']->value['link_rewrite']), ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subcategory']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" class="img">
                       <?php if ($_smarty_tpl->tpl_vars['subcategory']->value['id_image']) {?>
                       <?php if ($_smarty_tpl->tpl_vars['subcategory']->value['id_image']!="de-default") {?>

                       <div class="img-cat">
                           <img class="replace-2x" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCatImageLink($_smarty_tpl->tpl_vars['subcategory']->value['link_rewrite'],$_smarty_tpl->tpl_vars['subcategory']->value['id_image'],'medium_default'), ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subcategory']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" width="<?php echo $_smarty_tpl->tpl_vars['mediumSize']->value['width'];?>
" height="<?php echo $_smarty_tpl->tpl_vars['mediumSize']->value['height'];?>
" />
                       </div>
                       <?php }?>
                       <?php }?>

                       <div class="name"><?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate($_smarty_tpl->tpl_vars['subcategory']->value['name'],125,'...'), ENT_QUOTES, 'UTF-8', true);?>

                       </div>
                   </a>



               </li>
               <?php } ?>
           </ul>
       </div>
   </div>
   <?php }?>
   <?php } elseif ($_smarty_tpl->tpl_vars['products']->value) {?>
   <div class="block nav-cat">

    <h4 class="title_block"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</h4>
    <div class="block_content">
        <div class="row">
          <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayLeftColumn",'mod'=>"blocklayered"),$_smarty_tpl);?>

          <div class="col-sm-3"><div class="name"><?php echo smartyTranslate(array('s'=>"Sort by"),$_smarty_tpl);?>
</div><div class="con"><?php echo $_smarty_tpl->getSubTemplate ("./product-sort.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
</div></div>
          <div class="col-sm-3"><div class="name"><?php echo smartyTranslate(array('s'=>"Change category"),$_smarty_tpl);?>
</div><div class="con con-center"><a href="#" class="btn btn-default btn-cat"><i class="fa fa-th" aria-hidden="true"></i> <?php echo smartyTranslate(array('s'=>"Show all categories"),$_smarty_tpl);?>
</a></div></div>
      </div>
  </div>
</div>





<?php echo $_smarty_tpl->getSubTemplate ("./product-list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('products'=>$_smarty_tpl->tpl_vars['products']->value), 0);?>

<div class="content_sortPagiBar">
    <div class="bottom-pagination-content clearfix col-sm-12">
       <?php echo $_smarty_tpl->getSubTemplate ("./pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('paginationId'=>'bottom'), 0);?>

   </div>
</div>
<?php }?>
<?php }?>
<?php }?>

<?php if (!isset($_smarty_tpl->tpl_vars['products']->value)&&!isset($_smarty_tpl->tpl_vars['subcategories']->value)) {?>
<p class="alert alert-warning"><?php echo smartyTranslate(array('s'=>'This category is currently unavailable.'),$_smarty_tpl);?>
</p>
<?php }?>


</div>
<div class="advert-subpage">
    <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./our-atuts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./contact-section.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./customer-opinion.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</div>
<?php }} ?>
