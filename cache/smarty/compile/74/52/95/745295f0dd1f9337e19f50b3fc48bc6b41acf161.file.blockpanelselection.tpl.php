<?php /* Smarty version Smarty-3.1.19, created on 2017-06-07 16:33:20
         compiled from "/home/cloudpanel/htdocs/partcar.dev/modules/blockpanelselection/blockpanelselection.tpl" */ ?>
<?php /*%%SmartyHeaderCode:204245627659380eb070e3e8-82047666%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '745295f0dd1f9337e19f50b3fc48bc6b41acf161' => 
    array (
      0 => '/home/cloudpanel/htdocs/partcar.dev/modules/blockpanelselection/blockpanelselection.tpl',
      1 => 1462872310,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '204245627659380eb070e3e8-82047666',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'treecategories' => 0,
    'dysp_panel_vrm' => 0,
    'link' => 0,
    'dysp_panel_denmark' => 0,
    'dysp_panel_finland' => 0,
    'dysp_panel_france' => 0,
    'dysp_panel_netherlands' => 0,
    'dysp_panel_norway' => 0,
    'dysp_panel_sweden' => 0,
    'dysp_panel_kba' => 0,
    'dysp_panel_vin' => 0,
    'dysp_panel_italian' => 0,
    'dysp_panel_portugal' => 0,
    'dysp_panelcars' => 0,
    'id_lang' => 0,
    'id_shop' => 0,
    'list_years' => 0,
    'i' => 0,
    'select_year_car' => 0,
    'list_brands' => 0,
    'list_brand' => 0,
    'select_id_brand_car' => 0,
    'list_models' => 0,
    'list_model' => 0,
    'select_id_model_car' => 0,
    'list_types' => 0,
    'list_type' => 0,
    'select_id_type_car' => 0,
    'nnamebrand' => 0,
    'nnamemodel' => 0,
    'nnametype' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59380eb07f5a98_81327157',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59380eb07f5a98_81327157')) {function content_59380eb07f5a98_81327157($_smarty_tpl) {?>

<?php if (!$_smarty_tpl->tpl_vars['treecategories']->value) {?>
<!-- Block blockpanelselection module -->
<div id="blockpanelselection_block_left" class="block blockpanelselection">
	<h4 class="title_block"><span style="color : #233f92;">FIND</span> CAR PARTS</h4>
	
		<center style="padding:4px 0 0 0;"><span style="color:white;font-weight:bold;margin-top:4px;">ENTER REGISTRATION:</span></center>
		
		<!-- START show panel serch by VRM -->
		<?php if ($_smarty_tpl->tpl_vars['dysp_panel_vrm']->value) {?>		
		<div style="background: #ce2127; margin: 2px;">								
			<form method="get" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('search'), ENT_QUOTES, 'UTF-8', true);?>
" id="search_vrm_num" >								
				<p class="block_content" >
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_VRM_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="1" />					
					<input type="submit" id="search_button" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />
				</p>
			</form>									
		</div>
		<?php }?>
		<!-- END show panel serch by VRM -->
		
		<!-- START show panel serch by DENMARK -->
		<?php if ($_smarty_tpl->tpl_vars['dysp_panel_denmark']->value) {?>
		<div style="background: #ce2127; margin: 2px;">								
			<form method="get" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('search'), ENT_QUOTES, 'UTF-8', true);?>
" id="search_denmark_num">
				<p class="block_content">
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_Denmark_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="2" />					
					<input type="submit" id="search_button" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />
				</p>
			</form>
		</div>
		<?php }?>
		<!-- END show panel serch by DENMARK -->
		
		<!-- START show panel serch by FINLAND -->
		<?php if ($_smarty_tpl->tpl_vars['dysp_panel_finland']->value) {?>
		<div style="background: #ce2127; margin: 2px;">								
			<form method="get" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('search'), ENT_QUOTES, 'UTF-8', true);?>
" id="search_finland_num">
				<p class="block_content">
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_Finland_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="3" />					
					<input type="submit" id="search_button" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />
				</p>
			</form>
		</div>
		<?php }?>
		<!-- END show panel serch by FINLAND -->
		
		<!-- START show panel serch by FRANCE -->
		<?php if ($_smarty_tpl->tpl_vars['dysp_panel_france']->value) {?>
		<div style="background: #ce2127; margin: 2px;">								
			<form method="get" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('search'), ENT_QUOTES, 'UTF-8', true);?>
" id="search_france_num">
				<p class="block_content">
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_France_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="4" />					
					<input type="submit" id="search_button" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />
				</p>
			</form>
		</div>
		<?php }?>
		<!-- END show panel serch by FRANCE -->
		
		<!-- START show panel serch by NETHERLANDS -->
		<?php if ($_smarty_tpl->tpl_vars['dysp_panel_netherlands']->value) {?>
		<div style="background: #ce2127; margin: 2px;">								
			<form method="get" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('search'), ENT_QUOTES, 'UTF-8', true);?>
" id="search_netherlands_num">
				<p class="block_content">
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_Netherlands_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="5" />					
					<input type="submit" id="search_button" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />													
				</p>
			</form>
		</div>
		<?php }?>
		<!-- END show panel serch by NETHERLANDS -->
		
		<!-- START show panel serch by NORWAY -->
		<?php if ($_smarty_tpl->tpl_vars['dysp_panel_norway']->value) {?>
		<div style="background: #ce2127; margin: 2px;">								
			<form method="get" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('search'), ENT_QUOTES, 'UTF-8', true);?>
" id="search_norway_num">
				<p class="block_content">
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_Norway_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="6" />					
					<input type="submit" id="search_button" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />													
				</p>
			</form>
		</div>
		<?php }?>
		<!-- END show panel serch by NORWAY -->
		
		<!-- START show panel serch by SWEDEN -->
		<?php if ($_smarty_tpl->tpl_vars['dysp_panel_sweden']->value) {?>
		<div style="background: #ce2127; margin: 2px;">								
			<form method="get" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('search'), ENT_QUOTES, 'UTF-8', true);?>
" id="search_sweden_num">
				<p class="block_content">
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_Sweden_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="7" />					
					<input type="submit" id="search_button" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />													
				</p>
			</form>
		</div>
		<?php }?>
		<!-- END show panel serch by SWEDEN -->
		
		<!-- START show panel serch by KBA -->
		<?php if ($_smarty_tpl->tpl_vars['dysp_panel_kba']->value) {?>
		<div style="background: #ce2127; margin: 2px;">								
			<form method="get" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('search'), ENT_QUOTES, 'UTF-8', true);?>
" id="search_kba_num">
				<p class="block_content">
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_KBA_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="8" />					
					<input type="submit" id="search_button" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />													
				</p>
			</form>
		</div>
		<?php }?>
		<!-- END show panel serch by KBA -->
		
		<!-- START show panel serch by VIN -->
		<?php if ($_smarty_tpl->tpl_vars['dysp_panel_vin']->value) {?>
		<div style="background: #ce2127; margin: 2px;">								
			<form method="get" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('search'), ENT_QUOTES, 'UTF-8', true);?>
" id="search_vin_num">
				<p class="block_content">
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_VIN_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="9" />					
					<input type="submit" id="search_button" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />													
				</p>
			</form>
		</div>
		<?php }?>
		<!-- END show panel serch by VIN -->
		
		<!-- START show panel serch by ITALIAN -->
		<?php if ($_smarty_tpl->tpl_vars['dysp_panel_italian']->value) {?>
		<div style="margin-top: 2px;">								
			<form method="get" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('search'), ENT_QUOTES, 'UTF-8', true);?>
" id="search_italian_num">
				<p class="block_content">
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_Italian_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="10" />					
					<input type="submit" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />													
				</p>
			</form>
		</div>
		<?php }?>
		<!-- END show panel serch by ITALIAN -->
		
		<!-- START show panel serch by ITALIAN -->
		<?php if ($_smarty_tpl->tpl_vars['dysp_panel_portugal']->value) {?>
		<div style="background: #ce2127; margin: 2px;">								
			<form method="get" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('search'), ENT_QUOTES, 'UTF-8', true);?>
" id="search_potrugal_num">
				<p class="block_content">
					<input type="hidden" name="controller" value="search" />
					<input type="text" id="Edit_Portugal_num" class="textinput" name="plnum" style="width:170px;height:45px;text-transform:uppercase;" value placeholder="REG PLATE" maxlength="10" >																			
					<input type="hidden" name="bovsoftservice" value="11" />					
					<input type="submit" id="search_button" class="button_mini" style="width:45px; height:45px; top: 0px; color: white; background: #32CD32; font: 500 16px/18px Roboto, sans-serif;" value="GO >" />													
				</p>
			</form>
		</div>
		<?php }?>
		<!-- END show panel serch by ITALIAN -->
		
		<center style="padding:0 0 0 0;margin-top:-5px;"><span style="color:white;font-weight:bold;">----- OR -----</span></center>
		<center style="padding:0 0 0 0;margin-top:-8px;;margin-bottom:4px;"><span style="color:white;font-weight:bold;">SELECT YOUR VEHICLE:</span></center>			
	
	
	<div class="block_content">
		
		<!-- START show panel selection cars -->
		<?php if ($_smarty_tpl->tpl_vars['dysp_panelcars']->value) {?>	
			<!-- *********************** -->
					<select id="years_list" onchange="
																document.cookie = 'select_year_car=' + document.getElementById('years_list').value; 
																document.cookie = 'select_id_brand_car=0'; 
																document.cookie = 'select_id_model_car=0'; 
																document.cookie = 'select_id_type_car=0'; 
																getBrands(document.getElementById('years_list').value, <?php echo $_smarty_tpl->tpl_vars['id_lang']->value;?>
, <?php echo $_smarty_tpl->tpl_vars['id_shop']->value;?>
);
																 ">	
										<option value="0">Select year</option>
									<?php if ($_smarty_tpl->tpl_vars['list_years']->value) {?>
									<?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? count($_smarty_tpl->tpl_vars['list_years']->value)-1+1 - (0) : 0-(count($_smarty_tpl->tpl_vars['list_years']->value)-1)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 0, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
										<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['list_years']->value[$_smarty_tpl->tpl_vars['i']->value], ENT_QUOTES, 'UTF-8', true);?>
" <?php if ($_smarty_tpl->tpl_vars['list_years']->value[$_smarty_tpl->tpl_vars['i']->value]==$_smarty_tpl->tpl_vars['select_year_car']->value) {?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['list_years']->value[$_smarty_tpl->tpl_vars['i']->value], ENT_QUOTES, 'UTF-8', true);?>
</option>
									<?php }} ?>
									<?php }?>
							 </select>
			<!-- *********************** -->
					<select id="brand_list" onchange="
																document.cookie = 'select_id_brand_car=' + document.getElementById('brand_list').value; 
																document.cookie = 'select_id_model_car=0'; 
																document.cookie = 'select_id_type_car=0'; 
																getModels(document.getElementById('brand_list').value, document.getElementById('years_list').value, <?php echo $_smarty_tpl->tpl_vars['id_lang']->value;?>
, <?php echo $_smarty_tpl->tpl_vars['id_shop']->value;?>
);
																 ">
										<option value="0">Select make</option>
									<?php if ($_smarty_tpl->tpl_vars['list_brands']->value) {?>
									<?php  $_smarty_tpl->tpl_vars['list_brand'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['list_brand']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list_brands']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['list_brand']->key => $_smarty_tpl->tpl_vars['list_brand']->value) {
$_smarty_tpl->tpl_vars['list_brand']->_loop = true;
?>
										<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['list_brand']->value['id_bra'], ENT_QUOTES, 'UTF-8', true);?>
" <?php if ($_smarty_tpl->tpl_vars['list_brand']->value['id_bra']==$_smarty_tpl->tpl_vars['select_id_brand_car']->value) {?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['list_brand']->value['nameBrand'], ENT_QUOTES, 'UTF-8', true);?>
</option>
									<?php } ?>
									<?php }?>
							 </select>

			<!-- *********************** -->
					<select id="model_list" onchange="
																document.cookie = 'select_id_model_car=' + document.getElementById('model_list').value; 
																document.cookie = 'select_id_type_car=0'; 
																getTypes(document.getElementById('brand_list').value, document.getElementById('model_list').value, document.getElementById('years_list').value, <?php echo $_smarty_tpl->tpl_vars['id_lang']->value;?>
, <?php echo $_smarty_tpl->tpl_vars['id_shop']->value;?>
);
																 ">
										<option value="0">Select model</option>
									<?php if ($_smarty_tpl->tpl_vars['list_models']->value) {?>
									<?php  $_smarty_tpl->tpl_vars['list_model'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['list_model']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list_models']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['list_model']->key => $_smarty_tpl->tpl_vars['list_model']->value) {
$_smarty_tpl->tpl_vars['list_model']->_loop = true;
?>
										<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['list_model']->value['id_model'], ENT_QUOTES, 'UTF-8', true);?>
" <?php if ($_smarty_tpl->tpl_vars['list_model']->value['id_model']==$_smarty_tpl->tpl_vars['select_id_model_car']->value) {?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['list_model']->value['nameModel'], ENT_QUOTES, 'UTF-8', true);?>
</option>
									<?php } ?>
									<?php }?>
							 </select>

			<!-- *********************** -->
					<select style="width:70%;margin-bottom:10px;" id="type_list" onchange="
																document.cookie = 'select_id_type_car=' + document.getElementById('type_list').value; 
																getUrlcat(document.getElementById('type_list').value);
																">	
										<option value="0">Select type</option>
									<?php if ($_smarty_tpl->tpl_vars['list_types']->value) {?>
									<?php  $_smarty_tpl->tpl_vars['list_type'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['list_type']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list_types']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['list_type']->key => $_smarty_tpl->tpl_vars['list_type']->value) {
$_smarty_tpl->tpl_vars['list_type']->_loop = true;
?>
										<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['list_type']->value['id_type'], ENT_QUOTES, 'UTF-8', true);?>
" <?php if ($_smarty_tpl->tpl_vars['list_type']->value['id_type']==$_smarty_tpl->tpl_vars['select_id_type_car']->value) {?> selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['list_type']->value['nameType'], ENT_QUOTES, 'UTF-8', true);?>
</option>
									<?php } ?>
									<?php }?>
							 </select>

			<!-- *********************** -->				
								
								<!-- <button id="CleanDataCar" type="button" onclick="
																			document.cookie = 'select_year_car=0'; 
																			document.cookie = 'select_id_brand_car=0'; 
																			document.cookie = 'select_id_model_car=0'; 
																			document.cookie = 'select_id_type_car=0'; 
																			location.reload();
																			" name="" value="">
									Clean >
								</button> -->
								<!-- ------------------ -->
								<button id="OpenCar" type="button" onclick="
																	location.reload();
																	window.location='<?php if ($_smarty_tpl->tpl_vars['select_id_type_car']->value>0) {?><?php echo $_smarty_tpl->tpl_vars['link']->value->getCategoryLink((int)$_smarty_tpl->tpl_vars['select_id_type_car']->value);?>
<?php }?>'" 
																	name="" value=""> 
									Go >
								</button>
			<!-- *********************** -->

		<?php }?> 
		<!-- END show panel selection cars -->

	</div>
</div>
<!-- /Block blockpanelselection module -->
<?php }?>


<?php if ($_smarty_tpl->tpl_vars['treecategories']->value&&$_smarty_tpl->tpl_vars['treecategories']->value!='') {?>
	<!-- BEGIN TREE CATEGORIES -->
	<div id="blockpanelselection_block_left" class="block blockpanelselection" >  
		<h4 style="font-size:20px;" class="title_block">SELECTED VEHICLE</h4>
		<div class="block_content">
			<!-- <p style="padding:0;margin:0;margin-left:3px;"><strong style="color:red;"><u>SELECTED VEHICLE</u> :</strong></p> -->
			<p style="padding:0;margin:0;margin-left:3px;"><strong>Make:</strong> <?php echo $_smarty_tpl->tpl_vars['nnamebrand']->value;?>
</p>
			<p style="padding:0;margin:0;margin-left:3px;"><strong>Model:</strong> <?php echo $_smarty_tpl->tpl_vars['nnamemodel']->value;?>
</p>
			<p style="padding:0;margin:0;margin-left:3px;"><strong>Type:</strong> <?php echo $_smarty_tpl->tpl_vars['nnametype']->value;?>
</p>				
			<!-- --------------------- -->
			<p style="margin-top:3px;">										
				<button id="Deactivecar" type="button" class="button_mini" style="align : center; text-align: center; padding:0; margin-left:3px; width:50%; color: white; background: red;" 
						onclick="document.cookie = 'select_id_type_car=0'; 
								document.cookie = 'select_year_car=0'; 
								document.cookie = 'select_id_brand_car=0'; 
								document.cookie = 'select_id_model_car=0'; 								
								window.location = '<?php echo $_smarty_tpl->tpl_vars['link']->value->getBaseLink();?>
'" name="" value="">Change Vehicle</button>
			</p>	
		</div>
	</div>
	<div id="blockpanelselection_block_left" class="block blockpanelselection" >  
		<h4 style="font-size:20px;" class="title_block">   BROWSE CATEGORIES  </h4>
		<div class="block_content">
			<!-- --------------------- -->	
			<form method="get" action="
								javascript: d.openToShowAllNode();
								javascript: d.openToHideNodeExcept(document.getElementById('searchtree').value, true);" >			
				<p class="block_content">
						<input type="hidden" name="" value="search" />					
						<input type="text" id="searchtree" class="textinput_tree" name="tree" style="width:60%" value placeholder="Enter name part" >										
						<input type="submit" id="search_button" class="button_mini" style="width:70px; top: -2px; color: white; background: red;" value="Search >" />
				</p>
			</form>
			<!-- --------------------- -->	
				<p><a href="javascript: d.openAll();">Expand all</a> | <a href="javascript: d.closeAll();">Collapse all</a> | <strong><a href="javascript: d.openToShowAllNode();">Show all parts</a></strong></p>	
					<div class="block_content" id="dtree" style="height:750px;">											
						<script type="text/javascript">									
						<?php echo $_smarty_tpl->tpl_vars['treecategories']->value;?>

						</script>
					</div>

			<!-- --------------------- -->
		</div>
	</div>
	<!-- END TREE CATEGORIES -->
<?php }?>

<?php }} ?>
