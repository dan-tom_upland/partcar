<?php /* Smarty version Smarty-3.1.19, created on 2017-06-07 16:01:12
         compiled from "/home/cloudpanel/htdocs/partcar.dev/themes/upland/modules/blockcontactinfos/blockcontactinfos.tpl" */ ?>
<?php /*%%SmartyHeaderCode:835994145593807283d3473-69382274%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '56829c28894149562ca42df50ec813bb7fa2d57a' => 
    array (
      0 => '/home/cloudpanel/htdocs/partcar.dev/themes/upland/modules/blockcontactinfos/blockcontactinfos.tpl',
      1 => 1496423037,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '835994145593807283d3473-69382274',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'blockcontactinfos_phone' => 0,
    'blockcontactinfos_email' => 0,
    'tpl_uri' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59380728400d27_69144633',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59380728400d27_69144633')) {function content_59380728400d27_69144633($_smarty_tpl) {?><?php if (!is_callable('smarty_function_mailto')) include '/home/cloudpanel/htdocs/partcar.dev/tools/smarty/plugins/function.mailto.php';
?>

    <!-- MODULE Block contact infos -->
    <section id="block_contact_infos" class="footer-block">
     <div>
         <h4><?php echo smartyTranslate(array('s'=>'Contact','mod'=>'blockcontactinfos'),$_smarty_tpl);?>
</h4>
         <ul class="toggle-footer">
          <?php if ($_smarty_tpl->tpl_vars['blockcontactinfos_phone']->value!='') {?>
          <li>
              <span class="tel"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['blockcontactinfos_phone']->value, ENT_QUOTES, 'UTF-8', true);?>
</span>
              <small>8:00-16:00</small>
          </li>
          <?php }?>
          <?php if ($_smarty_tpl->tpl_vars['blockcontactinfos_email']->value!='') {?>
          <li>
              <strong><?php echo smartyTranslate(array('s'=>'Email:','mod'=>'blockcontactinfos'),$_smarty_tpl);?>
</strong>
              <span><?php echo smarty_function_mailto(array('address'=>htmlspecialchars($_smarty_tpl->tpl_vars['blockcontactinfos_email']->value, ENT_QUOTES, 'UTF-8', true),'encode'=>"hex"),$_smarty_tpl);?>
</span>
          </li>
          <?php }?>
      </ul>
      <div class="copy pull-right">© All right reserved <img src="<?php echo $_smarty_tpl->tpl_vars['tpl_uri']->value;?>
img/custom/fb-icon.svg" alt="facebook image"></div>
  </div>
</section>
<!-- /MODULE Block contact infos -->
<?php }} ?>
