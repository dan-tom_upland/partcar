<?php /* Smarty version Smarty-3.1.19, created on 2017-06-12 12:48:10
         compiled from "/home/cloudpanel/htdocs/partcar.dev/themes/upland/new-products.tpl" */ ?>
<?php /*%%SmartyHeaderCode:280826821593e6a4746c4d8-57284736%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b969f54dac50f5a60649289f29c64afec1fa7bcb' => 
    array (
      0 => '/home/cloudpanel/htdocs/partcar.dev/themes/upland/new-products.tpl',
      1 => 1497264488,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '280826821593e6a4746c4d8-57284736',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_593e6a474b45c6_17771313',
  'variables' => 
  array (
    'products' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_593e6a474b45c6_17771313')) {function content_593e6a474b45c6_17771313($_smarty_tpl) {?>

	<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?><?php echo smartyTranslate(array('s'=>'New products'),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

	<?php if ($_smarty_tpl->tpl_vars['products']->value) {?>



	<div class="block nav-cat">
		<h4 class="title_block"><?php echo smartyTranslate(array('s'=>'New products'),$_smarty_tpl);?>
</h4>
		<div class="block_content">
			<div class="row">
				<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayLeftColumn",'mod'=>"blocklayered"),$_smarty_tpl);?>

				<div class="col-sm-3"><div class="name"><?php echo smartyTranslate(array('s'=>"Sort by"),$_smarty_tpl);?>
</div><div class="con"><?php echo $_smarty_tpl->getSubTemplate ("./product-sort.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
</div></div>
				<div class="col-sm-3"></div>
			</div>
		</div>
	</div>


	<?php echo $_smarty_tpl->getSubTemplate ("./product-list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('products'=>$_smarty_tpl->tpl_vars['products']->value), 0);?>


	<div class="content_sortPagiBar">
		<div class="bottom-pagination-content clearfix">
			<?php echo $_smarty_tpl->getSubTemplate ("./pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('no_follow'=>1,'paginationId'=>'bottom'), 0);?>

		</div>
	</div>
	<?php } else { ?>
	<p class="alert alert-warning"><?php echo smartyTranslate(array('s'=>'No new products.'),$_smarty_tpl);?>
</p>
	<?php }?>
<?php }} ?>
